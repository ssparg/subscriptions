<?php

namespace Subscriptions\Http\Controllers;

use Illuminate\Http\Request;
use Subscriptions\Http\Requests;
use Subscriptions\Plan;

class PlanController extends Controller
{
    public function index()
    {
        return view('plans.index')->with([
            'plans' => Plan::get()
        ]);
    }

    public function show(Request $request, Plan $plan)
    {
        if ($request->user()->subscribedToPlan($plan->braintree_plan, 'main')) {
            return redirect()->route('home');
        }
        
        return view('plans.show')->with([
            'plan' => $plan
        ]);
    }
}
