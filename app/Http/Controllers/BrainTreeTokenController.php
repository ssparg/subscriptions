<?php

namespace Subscriptions\Http\Controllers;

use Illuminate\Http\Request;
use Subscriptions\Http\Requests;
use Braintree_ClientToken;

class BrainTreeTokenController extends Controller
{
    public function token()
    {
        return response()->json([
            'data' => [
                'token' => Braintree_ClientToken::generate(),
            ]
        ]);
    }
}
