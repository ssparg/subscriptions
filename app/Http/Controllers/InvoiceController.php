<?php

namespace Subscriptions\Http\Controllers;

use Illuminate\Http\Request;

use Subscriptions\Http\Requests;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        return view('invoices.index')->with([
            'invoices' => $request->user()->invoices(),
        ]);
    }

    public function download($invoiceId, Request $request)
    {
        return $request->user()->downloadInvoice($invoiceId, [
            'vendor' => 'shaunsparg ltd',
            'product' => 'Cookies',
        ]);
    }
}
