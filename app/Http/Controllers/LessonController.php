<?php

namespace Subscriptions\Http\Controllers;

use Illuminate\Http\Request;

use Subscriptions\Http\Requests;

class LessonController extends Controller
{
    public function index()
    {
        return view('lessons.index');
    }

    public function pro()
    {
        return 'pro lessons index';
    }
}
