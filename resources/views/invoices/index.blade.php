@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Subscriptions</div>

                <div class="panel-body">
                    @if ($invoices->count())
                        <table class="table table-striped">
                            <thead>
                                <th>Date</th>
                                <th>Amount</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach ($invoices as $invoice)
                                <tr>
                                    <td>{{ $invoice->date()->toDateTimeString() }}</td>
                                    <td>{{ $invoice->total() }}</td>
                                    <td><a href="{{ route('invoices.download', $invoice->id)}}">Download</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection
