@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $plan->name }}</div>

                <div class="panel-body">
                    <form method="post" action="{{ route('subscription.create') }}">
                      <div id="dropin-container"></div>
                      <hr>
                      {{ csrf_field()}}
                      <input type="hidden" name="plan" value="{{ $plan->id }}">
                      <button type="submit" class="btn btn-default hidden" id="payment_button">Pay</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://js.braintreegateway.com/js/braintree-2.27.0.min.js"></script>
    <script>
        $.ajax({
            url: '{{ route("braintree.token")}}'
        }).success(function(response){
            braintree.setup(response.data.token, 'dropin', {
              container: 'dropin-container',
              onReady: function() {
                $('#payment_button').removeClass('hidden')
              }
            });
        });
    </script>
@endsection